# Hostr

## About
Hostr is a project I started almost ten years ago when I set out to learn web development. Since then it's seen over 100,000 signups and served up over 2 billion file downloads.

It has been through many iterations, but in its current incarnation Hostr uses [Koa](http://koajs.com/) for the backend, [Angular](https://angular.io/) and [JSPM](http://jspm.io) for the frontend, and [Babel](https://babeljs.io/) for both.

## Getting Started

### Dependencies

Everything is taken care of by an `npm install`.

### Enviroment Variable Configuration

See [`.env.example`](.env.example). Copy it to `.env`, modify and `source .env` for development. [autoenv](https://github.com/kennethreitz/autoenv) is pretty nice for doing this automatically when you `cd` into your work directory.

### Deploying to Heroku

You'll need to add Heroku Redis and a MongoDB addon.

## Usage

### Start the app

```
$ npm start
```

This will install and build the frontend too.

Alternatively

```
$ npm run watch
```

Will watch your JS and CSS for changes, rebuild them, and reload the server.

### Run the tests

```
$ npm test
```

Running the tests will also set the indexes required for Mongo.

## Licence

My primary motivation is to get to work on Hostr in public. Contributions are welcome and all Javascript is Apache licenced, however the brand is not. The brand includes the name, logo images, CSS and marketing HTML.
